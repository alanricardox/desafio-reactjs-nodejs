export function setZip(zip) {
    return {
        type: 'SET_ZIP',
        payload: { zip }
    }
}

export function setAddressIsLoading() {
    return {
        type: 'SET_ADDRESS_IS_LOADING'
    }
}

export function setAddress(address) {
    return {
        type: 'SET_ADDRESS',
        payload: { address }
    }
}

export function clear() {
    return {
        type: 'CLEAR'
    }
}
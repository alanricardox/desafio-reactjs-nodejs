const express = require('express')

module.exports = function(server) {
    // URL base para toda a API
    const router = express.Router()
    server.use('/api', router)

    // ZIP Service
    const ZipService = require('../api/zip/zipService')
    router.get('/zip/:zip', (req, res) => {
        ZipService.getAddress(req.params)
            .then((response) => {
                let data = response.data
                res.json({
                    cep: data.cep ? data.cep : 'Não localizado',
                    logradouro: data.logradouro ? data.logradouro : 'Não localizado',
                    localidade: data.localidade ? data.localidade : 'Não localizado',
                    uf: data.uf ? data.uf : 'Não localizado'
                })
            })
            .catch(() => {
                res.sendStatus(406)
            })
    })
}
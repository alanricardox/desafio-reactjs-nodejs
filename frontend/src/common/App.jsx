import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import axios from 'axios'

import { Footer } from './Footer'
import { setZip, setAddressIsLoading, setAddress, clear } from './AppActions'

import style from './App.scss'

class App extends Component {
    constructor() {
        super()

        this.search = this.search.bind(this)
        this.setMask = this.setMask.bind(this)
    }

    setMask(mask) {
        let zip = document.getElementById('zip')
        let zipv = zip.value

        zip.value = zipv

        if (! /[0-9]$/.test(zipv.replace(/\D/, '')))
            zip.value = ''

        let i = zipv.length
        let out = mask.substring(1, 0)
        let text = mask.substring(i)
        if (text.substring(0, 1) != out)
            zip.value += text.substring(0, 1)

        zipv ? this.props.setZip(zipv) : this.props.clear()
    }

    search() {
        let zip = this.props.app.zip

        if (! /[0-9]{8}$/.test(zip.replace(/\D/, '')))
        {
            alert('Digite um CEP válido!')
            return false
        }

        this.props.setAddressIsLoading()
        this.props.setZip(zip)

        let self = this

        axios.get(`http://localhost:3003/api/zip/${zip}`)
            .then((response) => {
                self.props.setAddress(response.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    render() {
        return (
            <Fragment>
                <div className={[style.app, style.container].join(' ')}>
                    <div className={style.search_wrapper}>
                        <div className={style.search_wrapper__input_group}>
                            <input
                                id='zip'
                                type='text'
                                defaultValue={this.props.app.zip}
                                placeholder="00000-000"
                                aria-label="00000-000"
                                onKeyUp={() => this.setMask('#####-###')}
                                maxLength="9"
                                className={style.search_wrapper__input}
                            />
                            <button onClick={this.search}
                                className={style.search_wrapper__button}>Buscar CEP</button>
                        </div>
                    </div>
                        
                    { this.props.app.addressIsFilled ?
                        <div className={style.address_wrapper}>
                            <p><strong>CEP:</strong> {this.props.app.address.cep}</p>
                            <p><strong>Estado:</strong> {this.props.app.address.uf}</p>
                            <p><strong>Cidade:</strong> {this.props.app.address.localidade}</p>
                            <p><strong>Logradouro:</strong> {this.props.app.address.logradouro}</p>
                        </div>
                    : this.props.app.addressIsLoading ? 
                        <div className={style.address_wrapper}>
                            Carregando, aguarde..
                        </div> : null }
                    <Footer />
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return { app: state.app }
}

const mapDispatchToProps = dispatch => bindActionCreators({ setZip, setAddressIsLoading, setAddress, clear }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)
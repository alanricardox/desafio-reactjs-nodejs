import React from 'react'
import style from './App.scss'
import { APP_NAME } from '../env'

export const Footer = () => (
    <p className={style.footer_wrapper}>
        <small>
            {APP_NAME}
        </small>
    </p>
)
const INITIAL_STATE = {
    zip: null,
    addressIsFilled: false,
    addressIsLoading: false,
    address: {
        cep: null,
        logradouro: null,
        localidade: null,
        uf: null
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SET_ZIP':
            return { ...state, zip: action.payload.zip }
        case 'SET_ADDRESS_IS_LOADING':
            return { ...state, addressIsLoading: true }
        case 'SET_ADDRESS':
            return { ...state, address: action.payload.address, addressIsFilled: true, addressIsLoading: false }
        case 'CLEAR':
            return { ...state, zip: null, address: null, addressIsFilled: false }
        default:
            return state
    }
}
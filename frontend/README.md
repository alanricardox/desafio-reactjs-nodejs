# Desafio ReactJS + Redux + WebPack + NodeJS

## Autor

> Alan Ricardo de Pedro
> E-mail: alanricardox@gmail.com

## Introdução

> Código desenvolvido por Alan Ricardo de Pedro para um desafio de ReactJS + Redux + WebPack + NodeJS.

> O desafio é criar um formulário responsivo simples para consulta de CEP com Javascript, usando um endpoint intermediário em NodeJS :)

## Rodando o projeto Frontend

> Após clonar o projeto dê um: "npm install" na pasta "frontend".

> Após isso o app frontend pode ser "buildado" por meio do "npm run build" - os arquivos estarão disponíveis na pasta "/public".

> O app frontend também pode ser rodado em ambiente de dev com o comando: "npm run dev"

## Rodando o projeto Backend

> Após clonar o projeto dê um: "npm install" na pasta "backend".

> O app backend pode ser rodado em ambiente de dev com o comando: "npm run dev"
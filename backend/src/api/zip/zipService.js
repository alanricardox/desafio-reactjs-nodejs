const axios = require('axios')

exports.getAddress = (params) => {
    try {
        return axios.get(`https://viacep.com.br/ws/${params.zip}/json/`)
    } catch (err) {
        return err
    }
}